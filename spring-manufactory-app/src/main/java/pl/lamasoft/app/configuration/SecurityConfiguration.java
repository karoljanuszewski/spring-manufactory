package pl.lamasoft.app.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pl.lamasoft.app.security.DummyAuthenticationProvider;
import pl.lamasoft.service.Security.UserDetailService;

import java.util.List;


@Configuration
@EnableWebSecurity
class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/login*").anonymous()
                //.antMatchers("/admin/**").hasRole("ROLE_ADMIN")      //any files in /admin/ folder will be visible only for admin
                .antMatchers("/**").authenticated()
                .and().formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/admin_panel")
                .loginProcessingUrl("/perform_login")
                .failureUrl("/login?error")
                .usernameParameter("username")
                .passwordParameter("password")
                .and().logout().
                logoutUrl("/perform_logout").
                logoutSuccessUrl("/login?logout");
    }

    @Autowired
    protected void configureAuthenticationProviders(AuthenticationManagerBuilder authenticationManager, List<AuthenticationProvider> authenticationProviders) {
        authenticationProviders.forEach(authenticationManager::authenticationProvider);
    }

    @Bean
    @Profile("production")
    public AuthenticationProvider daoAuthenticationProvider(UserDetailService userDetailsService, PasswordEncoder passwordEncoder) {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);

        return daoAuthenticationProvider;
    }

    @Bean
    @Profile("development")
    public AuthenticationProvider dummyAuthenticationProvider() {
        return new DummyAuthenticationProvider();
    }

    @Bean
    public PlaintextPasswordEncoder passwordEncoder() {
        return new PlaintextPasswordEncoder();
    }

}
