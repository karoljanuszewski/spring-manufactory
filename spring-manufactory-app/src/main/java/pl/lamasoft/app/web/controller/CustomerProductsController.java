package pl.lamasoft.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.lamasoft.service.Product.ProductCommandService;
import pl.lamasoft.service.Product.ProductQueryService;

@Controller
public class CustomerProductsController {

    private static Logger LOGGER = LoggerFactory.getLogger(CustomerProductsController.class);


    private static final String VIEW_CUSTOMER_PANEL_PRODUCTS = "/customer/customer_panel_products";
    private static final String MODEL_LIST_CUSTOMER_PRODUCTS = "listCustomerProducts";
    private static final String MODEL_CUSTOMER_PRODUCT = "customerProduct";

    private final ProductCommandService productCommandService;
    private final ProductQueryService productQueryService;

    @Autowired
    public CustomerProductsController(ProductCommandService productCommandService, ProductQueryService productQueryService) {
        this.productCommandService = productCommandService;
        this.productQueryService = productQueryService;
    }


    @RequestMapping(value = "/customer/customerProducts/{customerId}", method = RequestMethod.GET)
    public String showAllCustomerProducts(@PathVariable("customerId") Long customerId, Model model) {

        LOGGER.debug("I AM A TEAPOT");


        model.addAttribute(MODEL_LIST_CUSTOMER_PRODUCTS, productQueryService.findAllProductsByCustomerId(customerId));

        return VIEW_CUSTOMER_PANEL_PRODUCTS;
    }

}
