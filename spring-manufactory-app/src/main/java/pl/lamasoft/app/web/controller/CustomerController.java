package pl.lamasoft.app.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.lamasoft.entity.Customer;
import pl.lamasoft.service.Customer.CustomerCommandService;
import pl.lamasoft.service.Customer.CustomerQueryService;
import pl.lamasoft.service.Security.UserDetailService;

import javax.validation.Valid;

@Controller
public class CustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private static final String VIEW_ADMIN_PANEL_CUSTOMERS = "/admin/admin_panel_customers";

    private static final String MODEL_LIST_CUSTOMERS = "listCustomers";
    private static final String MODEL_CUSTOMER = "customer";


    private final CustomerCommandService customerCommandService;
    private final CustomerQueryService customerQueryService;



    @Autowired
    public CustomerController(CustomerCommandService customerCommandService, CustomerQueryService customerQueryService) {
        this.customerCommandService = customerCommandService;
        this.customerQueryService = customerQueryService;

    }


    @RequestMapping(value ="/admin_panel_customers", method = RequestMethod.GET)
    public String showViewOfAllCustomersAndAddCustomer(@ModelAttribute("flash"+MODEL_CUSTOMER) Customer customer, Model model){
        LOGGER.debug("is executed");
        model.addAttribute(MODEL_CUSTOMER,customer);
        model.addAttribute(MODEL_LIST_CUSTOMERS, customerQueryService.findAllCustomers());



        return VIEW_ADMIN_PANEL_CUSTOMERS;
    }



    @RequestMapping(value = "/add_customer/save", method = RequestMethod.POST)
    public String saveCustomer(@Valid @ModelAttribute(MODEL_CUSTOMER) Customer customer,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX+MODEL_CUSTOMER, bindingResult);
            redirectAttributes.addFlashAttribute("flash"+MODEL_CUSTOMER, customer);



            return "redirect:/admin_panel_customers";
        }

        if (customer.getCustomerId() == null){

            customerCommandService.create(customer);
        }
        else {
            customerCommandService.update(customer);
        }


        return "redirect:/admin_panel_customers";
    }

    @RequestMapping("/customer/edit/{customerId}")
    public String getCustomer(@PathVariable("customerId") Long id, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute(MODEL_LIST_CUSTOMERS, customerQueryService.findAllCustomers());

        model.addAttribute(MODEL_CUSTOMER, customerQueryService.findCustomerById(id));



        return VIEW_ADMIN_PANEL_CUSTOMERS;
    }


    @RequestMapping("/customer/delete/{customerId}")
    public String deleteCustomer(@PathVariable("customerId") Long customerId){
        LOGGER.debug("is executed");
        customerCommandService.deleteCustomer(customerId);

        return "redirect:/admin_panel_customers";
    }
}
