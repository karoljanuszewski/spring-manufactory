package pl.lamasoft.app.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.lamasoft.entity.Product;
import pl.lamasoft.entity.Purchase;
import pl.lamasoft.service.Customer.CustomerQueryService;
import pl.lamasoft.service.Product.ProductCommandService;
import pl.lamasoft.service.Product.ProductQueryService;
import pl.lamasoft.service.Purchase.PurchaseCommandService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class ProductController {

     private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

     private static final String VIEW_ADMIN_PANEL_PRODUCTS = "/panel_products";
     private static final String MODEL_LIST_PRODUCTS = "listProducts";
     private static final String MODEL_PRODUCT = "product";
     private static final String MODEL_PURCHASE= "purchase";

     private final ProductCommandService productCommandService;
     private final ProductQueryService productQueryService;
     private final CustomerQueryService customerQueryService;
     private final PurchaseCommandService purchaseCommandService;


     @Autowired
     public ProductController(ProductCommandService productCommandService, ProductQueryService productQueryService, CustomerQueryService customerQueryService, PurchaseCommandService purchaseCommandService) {
        this.productCommandService = productCommandService;
        this.productQueryService = productQueryService;
         this.customerQueryService = customerQueryService;
         this.purchaseCommandService = purchaseCommandService;
     }

    /**
     * all methods in this class that return a VIEW should be implemented with two conditions : for ROLE_ADMIN and ROLE_USER
     */


    /**
     * This method should show a view of ALL products if "ROLE_ADMIN" is logged in
     * All clients have a "ROLE_USER". Every single client should be able to see the list of products ONLY available to them - NOT all the products in database
     * In order to find logged-in customerId, first, I get his login name from security context holder
     * the method getUserInSessionId() returns this attribute
     *
     */
    @RequestMapping(value ="/panel_products", method = RequestMethod.GET)
    public String showViewOfAllProductsAndAddProduct(@ModelAttribute("flash"+MODEL_PRODUCT) Product product, Model model, HttpServletRequest httpServletRequest){



        LOGGER.debug("is executed");
        model.addAttribute(MODEL_PRODUCT,product);
       // model.addAttribute(MODEL_PURCHASE,purchaseCommandService.findAllPurchases());
        if (httpServletRequest.isUserInRole("ROLE_ADMIN")){
            model.addAttribute(MODEL_LIST_PRODUCTS,productQueryService.findAllProducts());
        }
        else if (httpServletRequest.isUserInRole("ROLE_USER")){
            model.addAttribute(MODEL_LIST_PRODUCTS, productQueryService.findAllProductsByCustomerId(getUserInSessionId()));

        }

        return VIEW_ADMIN_PANEL_PRODUCTS;
    }


    /**
     *
     * @return this method returns id for user, who is logged in this application
     * this method is used in all methods that return a VIEW in this class
     */

    private Long getUserInSessionId() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userInSessionEmail = auth.getName();
        return customerQueryService.findIdByCustomerEmail(userInSessionEmail);
    }



    @RequestMapping(value = "/add_product/save", method = RequestMethod.POST)
    public String saveProduct(@Valid @ModelAttribute("product") Product product,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
        LOGGER.debug("CONTROLLER saveProduct() - executed!");

        if (httpServletRequest.isUserInRole("ROLE_USER")){
            product.setProductCustomer(customerQueryService.findCustomerById(getUserInSessionId()));
        }
        else if(httpServletRequest.isUserInRole("ROLE_ADMIN")){

            product.setProductCustomer(customerQueryService.findCustomerByHisProductId(product.getProductId()));

        }


        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.product", bindingResult);
            redirectAttributes.addFlashAttribute("flashProduct", product);

            return "redirect:/panel_products";
        }
        if (product.getProductId() == null){
            productCommandService.create(product);
        }
        else {
            productCommandService.update(product);
        }
        return "redirect:/panel_products";
    }



    @RequestMapping(value = "/product/addToCart",method = RequestMethod.POST)
    public String getViewOfProductsAndAddedProductToPurchase(@ModelAttribute("purchase") Model model, HttpServletRequest httpServletRequest, Long productId){

        Purchase purchase = new Purchase();

        purchase.setPurchaseProduct(productQueryService.findProductById(productId));

        if (purchase.getPurchaseId()==null){
            purchaseCommandService.create(purchase);
        }
        else {
            purchaseCommandService.update(purchase);
        }

        return "redirect:/panel_products";
    }






    @RequestMapping("/product/edit/{productId}")
    public String getCustomer(@PathVariable("productId") Long id, Model model, HttpServletRequest httpServletRequest) {
        LOGGER.debug("is executed!");

        model.addAttribute(MODEL_PRODUCT, productQueryService.findProductById(id));
        if (httpServletRequest.isUserInRole("ROLE_ADMIN")){
            model.addAttribute(MODEL_LIST_PRODUCTS, productQueryService.findAllProducts());
        }
        else if(httpServletRequest.isUserInRole("ROLE_USER")){
            model.addAttribute(MODEL_LIST_PRODUCTS,productQueryService.findAllProductsByCustomerId(getUserInSessionId()));
        }

        return VIEW_ADMIN_PANEL_PRODUCTS;
    }


    @RequestMapping("/product/delete/{productId}")
    public String deleteCustomer(@PathVariable("productId") Long productId){
        LOGGER.debug("is executed");
        productCommandService.deleteProduct(productId);

        return "redirect:/panel_products";
    }

}
