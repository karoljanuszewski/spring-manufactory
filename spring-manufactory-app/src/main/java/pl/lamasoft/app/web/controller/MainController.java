package pl.lamasoft.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.lamasoft.entity.Customer;
import pl.lamasoft.service.Customer.CustomerQueryService;
import pl.lamasoft.service.Customer.CustomerCommandService;

import javax.validation.Valid;


@Controller
public class MainController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private final CustomerCommandService customerCommandService;

    private final CustomerQueryService customerQueryService;





    @Autowired
    public MainController(CustomerCommandService customerCommandService, CustomerQueryService customerQueryService) {
        this.customerCommandService = customerCommandService;
        this.customerQueryService = customerQueryService;


    }

    @RequestMapping(value = "/")
    public String index() {
        LOGGER.info(" executed");
        return "/admin/admin_panel";
    }

    @RequestMapping(value = "/admin_panel")
    public String adminPanel(){
        LOGGER.info(" executed");
        return "/admin/admin_panel";
    }

    @RequestMapping(value = "/*")
    public String pageNotFound(){
        LOGGER.info(" executed");
        return "/common/page_not_found";
    }







}
