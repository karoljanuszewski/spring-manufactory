package pl.lamasoft.app.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.lamasoft.entity.Product;
import pl.lamasoft.entity.Purchase;
import pl.lamasoft.service.Customer.CustomerCommandService;
import pl.lamasoft.service.Purchase.PurchaseCommandService;

@Controller
public class PurchaseController {

    private final PurchaseCommandService purchaseCommandService;
    private final CustomerCommandService customerCommandService;


    @Autowired
    public PurchaseController(PurchaseCommandService purchaseCommandService, CustomerCommandService customerCommandService) {
        this.purchaseCommandService = purchaseCommandService;
        this.customerCommandService = customerCommandService;
    }


    @RequestMapping(value = "/product/purchase", method = RequestMethod.POST)
    public String addNewPurchase(@ModelAttribute("purchase")  Product product, Long productId, Model model){
        Long id = product.getProductId();

        Long asdasd = productId;

        Purchase purchase = new Purchase();
        purchase.setPurchaseProduct(product);
        model.addAttribute("purchase",purchase);



        return "";
    }

}
