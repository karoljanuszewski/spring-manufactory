<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Logowanie</title>
    <jsp:include page="/views/common/header.jsp"/>
</head>
<body>

<jsp:include page="/views/common/style.jsp"/>
<jsp:include page="../common/navbar.jsp"/>
<security:authorize access="hasRole('ROLE_ADMIN')">
    <div class="container">


        <div class="container">

            <h2><spring:message code="admin.panel.customers.title"/></h2>


            <div class="container">

                <h3><spring:message code="add.customer.title"/></h3>

                <spring:url var="saveAction" value="/add_customer/save"/>

                <form:form method="post" modelAttribute="customer" action="${saveAction}" class="form-horizontal">

                    <form:hidden path="customerId"/>


                    <spring:bind path="customerName">
                        <div class="form-group" ${status.error ? 'has-error' : ''}>
                            <form:label path="customerName" class="col-sm-2 control-label">
                                <spring:message code="add.customer.form.customer.name"/>
                            </form:label>
                            <div class="col-sm-10">
                                <form:input path="customerName" class="form-controll"/>
                                <form:errors path="customerName" class="controll-label"/>
                            </div>
                        </div>
                    </spring:bind>


                    <spring:bind path="customerAddress">
                        <div class="form-group" ${status.error ? 'has-error' : ''}>
                            <form:label path="customerAddress" class="col-sm-2 control-label">
                                <spring:message code="add.customer.form.address"/>
                            </form:label>
                            <div class="col-sm-10">
                                <form:input path="customerAddress" class="form-controll"/>
                                <form:errors path="customerAddress" class="controll-label"/>
                            </div>
                        </div>
                    </spring:bind>


                    <spring:bind path="customerTel">
                        <div class="form-group" ${status.error ? 'has-error' : ''}>
                            <form:label path="customerTel" class="col-sm-2 control-label">
                                <spring:message code="add.customer.form.tel"/>
                            </form:label>
                            <div class="col-sm-10">
                                <form:input path="customerTel" class="form-controll"/>
                                <form:errors path="customerTel" class="controll-label"/>
                            </div>
                        </div>
                    </spring:bind>



                    <spring:bind path="userAccount.userAccountEmail">
                        <div class="form-group" ${status.error ? 'has-error' : ''}>
                            <form:label path="userAccount.userAccountEmail" class="col-sm-2 control-label">
                                <spring:message code="add.customer.form.email"/>
                            </form:label>
                            <div class="col-sm-10">
                                <form:input path="userAccount.userAccountEmail" class="form-controll"/>
                                <form:errors path="userAccount.userAccountEmail" class="controll-label"/>
                            </div>
                        </div>
                    </spring:bind>


                    <spring:bind path="userAccount.userAccountPassword">
                        <div class="form-group" ${status.error ? 'has-error' : ''}>
                            <form:label path="userAccount.userAccountPassword" class="col-sm-2 control-label">
                                <spring:message code="admin_panel.form.clientpassword"/>
                            </form:label>
                            <div class="col-sm-10">
                                <form:input path="userAccount.userAccountPassword" class="form-controll"/>
                                <form:errors path="userAccount.userAccountPassword" class="controll-label"/>
                            </div>
                        </div>
                    </spring:bind>

                    <div class="container">
                    <button class="btn btn-info" type="submit">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <spring:message code="add.customer.form.submit"/>
                    </button>
                    </div>


                </form:form>
            </div>


            <div class="table-responsive">
                <h3><spring:message code="customer.list.table.title"/></h3>
                <table id="aTable" class="table table-condensed">
                    <thead>
                    <tr>
                        <th onclick="sortTable(0)"><spring:message code="add.customer.form.customer.name"/></th>
                        <th onclick="sortTable(1)"><spring:message code="add.customer.form.address"/></th>
                        <th onclick="sortTable(2)"><spring:message code="add.customer.form.email"/></th>
                        <th onclick="sortTable(3)"><spring:message code="add.customer.form.tel"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:choose>
                        <c:when test="${!empty listCustomers}">
                            <c:forEach items="${listCustomers}" var="customer">
                                <tr>
                                    <td>${customer.customerName}</td>
                                    <td>${customer.customerAddress}</td>
                                    <td>${customer.userAccount.userAccountEmail}</td>
                                    <td>${customer.customerTel}</td>


                                    <td>
                                        <spring:url value="/customer/customerProducts/${customer.customerId}"
                                                    var="customerProductsUrl"/>

                                        <spring:url value="/customer/edit/${customer.customerId}"
                                                    var="customerEditUrl"/>

                                        <spring:url value="/customer/delete/${customer.customerId}"
                                                    var="customerDeleteUrl"/>

                                        <button class="btn btn-info" onclick="location.href='${customerProductsUrl}'">
                                            pokaz produkty klienta
                                        </button>

                                        <button class="btn btn-info" onclick="location.href='${customerDeleteUrl}'">
                                            <span class="glyphicon glyphicon-trash"></span>
                                            <spring:message code="customer.list.action.delete"/>
                                        </button>
                                        <button class="btn btn-info" onclick="location.href='${customerEditUrl}'">
                                            <span class="glyphicon glyphicon-edit"></span>
                                            <spring:message code="customer.list.action.update"/>
                                        </button>
                                    </td>


                                </tr>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td><spring:message code="list.customer.empty"/></td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>
            </div>


        </div>

    </div>
    <jsp:include page="/views/common/sortTable_script.jsp"/>
    <jsp:include page="/views/common/footer.jsp"/>
</security:authorize>
</body>
</html>