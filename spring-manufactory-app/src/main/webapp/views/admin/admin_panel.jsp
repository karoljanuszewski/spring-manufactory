<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Title</title>

    <jsp:include page="/views/common/header.jsp"/>
</head>
<body>
<jsp:include page="/views/common/style.jsp"/>
<jsp:include page="../common/navbar.jsp"/>




<div class="container">

    <jsp:include page="/views/common/banner.jsp"/>

</div>

<jsp:include page="/views/common/footer.jsp"/>
</body>
</html>
