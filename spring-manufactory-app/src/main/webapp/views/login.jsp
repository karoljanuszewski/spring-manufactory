<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Logowanie</title>

    <jsp:include page="common/header.jsp"/>

</head>
<body>
<div class="container">
    <%-- <jsp:include page="common/banner.jsp"/>

     <jsp:include page="common/language.jsp"/>--%>

    <div class="container">
        <h3><spring:message code="login.title"/></h3>

        <c:if test="${not empty error}">
            <div class="error">${error}</div>
        </c:if>
        <c:if test="${not empty msg}">
            <div class="msg">${msg}</div>
        </c:if>

        <spring:url var="login" value="perform_login"/>

        <form:form method="post" modelAttribute="login" action="${login}" class="form-horizontal">

            <spring:bind path="username">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:label path="username" class="col-sm-2 control-label">
                        <spring:message code="login.label.username"/>
                    </form:label>
                    <div class="col-sm-10">
                        <form:input path="username" class="form-control"/>
                        <form:errors path="username" class="control-label"/>
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="password">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:label path="password" class="col-sm-2 control-label">
                        <spring:message code="login.label.password"/>
                    </form:label>
                    <div class="col-sm-10">
                        <form:input path="password" type="password" class="form-control"/>
                        <form:errors path="password" class="control-label"/>
                    </div>
                </div>
            </spring:bind>

            <button class="btn btn-primary" type="submit">
                <spring:message code="login.submit.label"/>
            </button>
        </form:form>
    </div>
</div>
</body>
</html>