<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>



<div class="navbar navbar-inverse navbar-fixed-left">
    <a class="navbar-brand" href="#"><spring:message code="admin.panel.link.title"/></a>
    <ul class="nav navbar-nav">

        <security:authorize access="hasRole('ROLE_ADMIN')">
            <li>
                <div class="container"><a class="btn btn-primary" href="admin_panel_customers" role="button">
                    <span class="glyphicon glyphicon-user"></span>
                    <spring:message code="admin.panel.customers.link.title"/></a></div>
            </li>
        </security:authorize>

        <li>
            <div class="container"><a class="btn btn-primary" href="panel_products" role="button">
                <span class="glyphicon glyphicon-oil"></span>
                <spring:message code="admin.panel.products.link.title"/></a></div>
        </li>

        <li>
            <div class="container"><a class="btn btn-primary" href="#" role="button">Link3</a></div>
        </li>
        <li>

            <div class="container"><a class="btn btn-primary" href="#" role="button">Link4</a></div>
        </li>
        <li>
            <jsp:include page="/views/common/logout.jsp"/>
        </li>
    </ul>


</div>





