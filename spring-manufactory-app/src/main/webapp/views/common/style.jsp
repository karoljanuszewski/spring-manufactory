<style>
    body {font-family: Arial, Helvetica, sans-serif;}


    /*STYLES FOR MODAl - for opening descripions*/
    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }



    /*STYLES FOR NAVBAR*/

    .navbar-fixed-left {
        width: 200px;
        position: fixed;
        border-radius: 0;
        height: 100%;
    }

    .navbar-fixed-left .navbar-nav > li {
        color: white;
        margin-top: 40px;
        margin-bottom: 40px;
        margin-left: 20px;
        float: none; /* Cancel default li float: left */
        width: 150px; /*szerokość guzików*/
    }

    /* to powoduje, że treść kontenera po prawej nie miesza się z navbarem po lewej przy zwiększaniu widoku*/
    .navbar-fixed-left + .container {
        padding-left: 160px;
    }

    .btn-primary {
        width: 120px;
    }

    th {
        cursor: pointer;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2
    }

    body {font-family: Arial, Helvetica, sans-serif;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }



    /*STYLES FOR TABLE SORT*/
    th {
        cursor: pointer;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2
    }


</style>