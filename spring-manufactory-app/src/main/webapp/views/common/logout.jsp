<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="container">
    <spring:url var="logoutUrl" value="/perform_logout"/>

    <form:form method="post" action="${logoutUrl}" class="form-horizontal">
        <button class="btn btn-primary" type="submit">
            <span class="glyphicon glyphicon-off"></span>
            <spring:message code="logout.submit.label"/>
        </button>
    </form:form>
</div>