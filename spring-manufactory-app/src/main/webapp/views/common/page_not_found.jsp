<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>404</title>
    <jsp:include page="header.jsp"/>
</head>
<body>
<jsp:include page="/views/common/navbar.jsp"/>
<div class="container">
    <h1><spring:message code="page.not.found.message"/></h1>




</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
