<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
    <spring:url var="bootstrapCss" value="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${bootstrapCss}">

</head>
<body>

<div class="container">

    <a class="btn btn-primary" href="add_customer" role="button">
        <spring:message code="add.customer.link.title"/></a>

    <a class="btn btn-primary" href="show_all_customers" role="button">
        <spring:message code="show.all.customers.link.title"/></a>

    <a class="btn btn-primary" href="add_product" role="button">
        <spring:message code="add.product.link.title"/></a>

    </a><a class="btn btn-primary" href="show_all_products" role="button">
    <spring:message code="show.all.products.link.title"/></a>



</div>


</body>
</html>
