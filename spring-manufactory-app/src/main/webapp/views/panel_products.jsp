<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <jsp:include page="/views/common/header.jsp"/>
</head>
<body>
<jsp:include page="/views/common/style.jsp"/>
<jsp:include page="common/navbar.jsp"/>


<div class="container">


    <div class="container">
        <h2><spring:message code="admin.panel.product.title"/></h2>

        <div class="container">
            <h3>
                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <spring:message code="add.product.title.admin"/>
                </security:authorize>
                <security:authorize access="hasRole('ROLE_USER')">
                    <spring:message code="add.product.title.customer"/>
                </security:authorize>
            </h3>
            <spring:url var="saveAction" value="/add_product/save"/>
            <form:form method="post" modelAttribute="product" action="${saveAction}" class="form-horizontal">

                <form:hidden path="productId"/>
                <spring:bind path="productName">
                    <div class="form-group" ${status.error ? 'has-error' : ''}>
                        <form:label path="productName" class="col-sm-2 control-label">
                            <spring:message code="add.product.form.product.name"/>
                        </form:label>
                        <div class="col-sm-10">
                            <form:input path="productName" class="form-controll"/>
                            <form:errors path="productName" class="controll-label"/>
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="productDesc">
                    <div class="form-group" ${status.error ? 'has-error' : ''}>
                        <form:label path="productDesc" class="col-sm-2 control-label">
                            <spring:message code="add.product.form.product.desc"/>
                        </form:label>
                        <div class="col-sm-10">
                            <form:input path="productDesc" class="form-controll"/>
                            <form:errors path="productDesc" class="controll-label"/>
                        </div>
                    </div>
                </spring:bind>

                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <spring:bind path="productQuantity">
                        <div class="form-group" ${status.error ? 'has-error' : ''}>
                            <form:label path="productQuantity" class="col-sm-2 control-label">
                                <spring:message code="add.product.form.product.quantity"/>
                            </form:label>
                            <div class="col-sm-10">
                                <form:input path="productQuantity" class="form-controll"/>
                                <form:errors path="productQuantity" class="controll-label"/>
                            </div>
                        </div>
                    </spring:bind>
                </security:authorize>


                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <spring:bind path="productUnitPrice">
                        <div class="form-group" ${status.error ? 'has-error' : ''}>
                            <form:label path="productUnitPrice" class="col-sm-2 control-label">
                                <spring:message code="add.product.form.product.price"/>
                            </form:label>
                            <div class="col-sm-10">
                                <form:input path="productUnitPrice" class="form-controll"/>
                                <form:errors path="productUnitPrice" class="controll-label"/>
                            </div>
                        </div>
                    </spring:bind>
                </security:authorize>


                <div class="container">
                    <button class="btn btn-info" type="submit">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <security:authorize access="hasRole('ROLE_ADMIN')">
                            <spring:message code="add.product.title.admin"/>
                        </security:authorize>
                        <security:authorize access="hasRole('ROLE_USER')">
                            <spring:message code="add.product.title.customer"/>
                        </security:authorize>
                    </button>
                </div>
            </form:form>
        </div>

        <div class="table-responsive">
            <h3><spring:message code="show.all.products.title"/></h3>
            <table id="aTable" class="table table-condensed">
                <thead>
                <tr>
                    <security:authorize access="hasRole('ROLE_ADMIN')">
                        <th onclick="sortTable(0)"><spring:message code="add.customer.form.customer.name"/></th>
                    </security:authorize>
                    <th onclick="sortTable(1)"><spring:message code="add.products.form.name"/></th>
                    <security:authorize access="hasRole('ROLE_ADMIN')">
                        <th onclick="sortTable(2)"><spring:message code="add.products.form.quantity"/></th>
                    </security:authorize>
                    <th onclick="sortTable(3)"><spring:message code="add.produtcs.form.price"/></th>
                    <th onclick="sortTable(4)"><spring:message code="add.produtcs.form.desc"/></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${!empty listProducts}">
                        <c:forEach items="${listProducts}" var="product">
                            <c:url var="purchaseProduct" value="/product/purchase/"/>

                            <form:form method="post" action="${purchaseProduct}">
                                <tr>

                                    <security:authorize access="hasRole('ROLE_ADMIN')">
                                        <td>${product.productCustomer.customerName}</td>
                                    </security:authorize>
                                    <td>${product.productName}</td>
                                    <security:authorize access="hasRole('ROLE_ADMIN')">
                                        <td>${product.productQuantity}</td>
                                    </security:authorize>
                                    <td>${product.productUnitPrice}</td>
                                    <td>
                                        <button id="aBtn">
                                            <span class="glyphicon glyphicon-file"></span>
                                            <spring:message code="product.read.description"/>
                                        </button>
                                        <div id="aModal" class="modal">
                                            <div class="modal-content">
                                                <span class="close">&times;</span>
                                                    ${product.productDesc}
                                            </div>
                                        </div>
                                    </td>



                                            <%--
                                                ten przycisk powinien wrzucac product.producId do modelu purchase
                                                --%>


                                    <td><input type="hidden" name="productId" value="${product.productId}">
                                    </td>

                                    <td>
                                        <button class="btn btn-info" type="submit">


                                            <span class="glyphicon glyphicon-shopping-cart"></span>
                                            <spring:message code="product.add.to.cart"/>
                                        </button>

                                    </td>

                                    <td>
                                        <spring:url value="/product/edit/${product.productId}" var="productEditUrl"/>

                                        <spring:url value="/product/delete/${product.productId}"
                                                    var="productDeleteUrl"/>

                                        <button class="btn btn-info" onclick="location.href='${productDeleteUrl}'">
                                            <span class="glyphicon glyphicon-trash"></span>
                                            <spring:message code="product.list.action.delete"/>
                                        </button>

                                        <security:authorize access="hasRole('ROLE_ADMIN')">
                                            <button class="btn btn-info" onclick="location.href='${productEditUrl}'">
                                                <span class="glyphicon glyphicon-edit"></span>
                                                <spring:message code="product.list.action.update"/>
                                            </button>
                                        </security:authorize>
                                    </td>


                                </tr>
                            </form:form>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td><spring:message code="list.products.empty"/></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
        </div>

        <%--??TODO
                <div class="container">
                    <h3><spring:message code="panel.cart.title"/></h3>
                    <div class="table-responsive">
                        <spring:url var="purchasePlaceOrder" value="/purchase/placeOrder"/>
                        <form:form method="post" modelAttribute="purchase" action="${purchasePlaceOrder}">
                            <table class="table-condensed">
                                <thead>
                                <tr>
                                    <th><spring:message code="purchase.product.name"/></th>


                                    <th><spring:message code="purchase.product.unit.price"/></th>
                                    <th><spring:message code="purchase.quantity"/></th>
                                    <th><spring:message code="purchase.discount"/></th>
                                    <th><spring:message code="purchase.unit.ptrice"/></th>
                                    <th><spring:message code="purchase.price"/></th>

                                </tr>
                                </thead>

                                <tbody>
                                <c:choose>
                                    <c:when test="${!empty purchase}">
                                        <c:forEach items="${purchase}" var="product">
                                            <tr>
                                                <form:hidden path="purchase.productId"/>
                                                <td>${purchase.product.productName}</td>
                                                <spring:bind path="purchase.product.productName">
                                                    <div  ${status.error ? 'has-error' : ''}>
                                                            <form:input path="productName" class="form-controll"/>
                                                            <form:errors path="productName" class="controll-label"/>

                                                    </div>
                                                </spring:bind>
                                                <td>${purchase.product.productUnitPrice}</td>
                                                <td>${purchase.purchaseQuantity}</td>
                                                <td>${purchase.purchaseDiscount}</td>
                                                <td>${purchase.purchaseProductUnitPrice}</td>
                                                <td>${purchase.purchasePrice}</td>

                                                <td>
                                                    <button class="btn btn-info" onclick="">
                                                        przelicz
                                                    </button>

                                                </td>

                                                <td>
                                                    <button class="btn btn-info" onclick="">
                                                        zamow
                                                    </button>

                                                </td>


                                            </tr>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <td><spring:message code="purchase.list.empty"/></td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                                </tbody>


                            </table>
                        </form:form>
                    </div>
                </div>--%>


    </div>
</div>
<jsp:include page="/views/common/sortTable_script.jsp"/>
<jsp:include page="/views/common/modal_script.jsp"/>
<jsp:include page="/views/common/footer.jsp"/>
</body>
</html>
