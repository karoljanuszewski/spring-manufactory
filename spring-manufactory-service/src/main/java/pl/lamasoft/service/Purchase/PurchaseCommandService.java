package pl.lamasoft.service.Purchase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lamasoft.entity.Purchase;
import pl.lamasoft.repository.PurchaseRepository;
import pl.lamasoft.service.Exception.PurchaseNotFoundException;

import java.util.List;

@Service
@Transactional
public class PurchaseCommandService {

    private final PurchaseRepository purchaseRepository;


    @Autowired
    public PurchaseCommandService(PurchaseRepository purchaseRepository){
        this.purchaseRepository = purchaseRepository;
    }

    public Long create(Purchase purchase) {
        purchaseRepository.save(purchase);
        return purchase.getPurchaseId();
    }

    public void update(Purchase purchase) {

       Purchase dbPurchase = purchaseRepository.findOne(purchase.getPurchaseId());
       if (dbPurchase==null){
           throw new PurchaseNotFoundException();
       }
       dbPurchase.setPurchaseProduct(purchase.getPurchaseProduct());
       dbPurchase.setPurchaseDiscount(purchase.getPurchaseDiscount());
       dbPurchase.setPurchasePrice(purchase.getPurchasePrice());
       dbPurchase.setPurchaseProductUnitPrice(purchase.getPurchaseProductUnitPrice());
       dbPurchase.setPurchaseQuantity(purchase.getPurchaseQuantity());

    }

    public List<Purchase> findAllPurchases() {
        return  purchaseRepository.findAll();
    }
}
