package pl.lamasoft.service.Product;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lamasoft.entity.Product;
import pl.lamasoft.repository.ProductRepository;
import pl.lamasoft.service.Customer.CustomerQueryService;
import pl.lamasoft.service.Exception.ProductNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class ProductQueryService {

    private final ProductRepository productRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerQueryService.class);
    @Autowired
    public ProductQueryService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public List<Product> findAllProducts(){
        return productRepository.findAll();
    }

    public Product findProductById(Long id) {
        
        Product product = productRepository.findOne(id);
        if(product==null){
            LOGGER.debug("Customer with id "+ id +" not found");
            throw  new ProductNotFoundException();
        }
        return product;
    }


    public List<Product> findAllProductsByCustomerId(Long customerId) {

        return productRepository.findAll().stream()
                .filter(product ->product.getProductCustomer().getCustomerId().equals(customerId))
                .collect(Collectors.toList());
    }

}
