package pl.lamasoft.service.Product;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lamasoft.entity.Product;
import pl.lamasoft.repository.ProductRepository;
import pl.lamasoft.service.Exception.ProductNotFoundException;

@Service
@Transactional
public class ProductCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductCommandService.class);

    private final ProductRepository productRepository;

    @Autowired
    public ProductCommandService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Long create(Product product){

        productRepository.save(product);
        return product.getProductId();
    }

    public void deleteProduct(Long productId) {
        Product productToDelete = productRepository.findOne(productId);
        if (productToDelete == null) {
            LOGGER.debug("UserAccount with id " + productId + " not found.");
            throw new ProductNotFoundException();
        }
        productRepository.delete(productToDelete);
    }

    public void update(Product product) {
        Product dbProduct = productRepository.findOne(product.getProductId());
        if (dbProduct == null) {
            LOGGER.debug("customer with id" + product.getProductId() + "not found");
            throw new ProductNotFoundException();
        }
        dbProduct.setProductName(product.getProductName());
        dbProduct.setProductDesc(product.getProductDesc());
        dbProduct.setProductUnitPrice(product.getProductUnitPrice());
        dbProduct.setProductQuantity(product.getProductQuantity());
    }



}
