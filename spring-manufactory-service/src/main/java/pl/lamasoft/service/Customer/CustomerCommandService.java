package pl.lamasoft.service.Customer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lamasoft.entity.Customer;
import pl.lamasoft.entity.Role;
import pl.lamasoft.repository.CustomerRepository;
import pl.lamasoft.repository.UserAccountRepository;
import pl.lamasoft.service.Exception.CustomerNotFoundException;

@Service
@Transactional
public class CustomerCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerCommandService.class);

    private final CustomerRepository customerRepository;
    private final UserAccountRepository userAccountRepository;

    @Autowired
    public CustomerCommandService(CustomerRepository customerRepository, UserAccountRepository userAccountRepository) {
        this.customerRepository = customerRepository;
        this.userAccountRepository = userAccountRepository;
    }

    public Long create(Customer customer) {
        customer.getUserAccount().setUserAccountRole(Role.ROLE_USER);       //every single new Customer will be created with ROLE_USER
        userAccountRepository.save(customer.getUserAccount());
        customerRepository.save(customer);

        return customer.getCustomerId();
    }


    public void deleteCustomer(Long customerId) {
        Customer customerToDelete = customerRepository.findOne(customerId);
        if (customerToDelete == null) {
            LOGGER.debug("UserAccount with id " + customerId + " not found.");
            throw new CustomerNotFoundException();
        }
        customerRepository.delete(customerToDelete);
    }

    public void update(Customer customer) {
        Customer dbCustomer = customerRepository.findOne(customer.getCustomerId());
        if (dbCustomer == null) {
            LOGGER.debug("customer with id" + customer.getCustomerId() + "not found");
            throw new CustomerNotFoundException();
        }
        dbCustomer.setCustomerName(customer.getCustomerName());
        dbCustomer.setCustomerAddress(customer.getCustomerAddress());
        dbCustomer.setCustomerTel(customer.getCustomerTel());
    }
}
