package pl.lamasoft.service.Customer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lamasoft.entity.Customer;
import pl.lamasoft.entity.Product;
import pl.lamasoft.repository.CustomerRepository;
import pl.lamasoft.repository.ProductRepository;
import pl.lamasoft.service.Exception.CustomerNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;


@Service
@Transactional(readOnly=true)
public class CustomerQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerQueryService.class);

    private final CustomerRepository customerRepository;

    private final ProductRepository productRepository;

    @Autowired
    public CustomerQueryService(CustomerRepository customerRepository, ProductRepository productRepository){
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
    }


    public List<Customer> findAllCustomers() {

        return customerRepository.findAll();
    }

    public Customer findCustomerById(Long id) {

        //TODO check if findOne() finds by customerId or something else
        Customer customer = customerRepository.findOne(id);
        if(customer==null){
            LOGGER.debug("Customer with id "+ id +" not found");
            throw  new CustomerNotFoundException();
        }
        return customer;
    }


    public Long findIdByCustomerEmail(String userInSessionEmail) {


        return customerRepository.findAll().stream()
                .filter(customer -> customer.getUserAccount().getUserAccountEmail().equals(userInSessionEmail))
                .findAny()
                .get()
                .getCustomerId();
    }

    public Customer findCustomerByHisProductId(Long productId) {


        /**
         * the list was created because the stream's result could be only Optional<Product> or a List<Product>
         */
        List<Product> productList=
        productRepository.findAll().stream()
                .filter(product -> product.getProductId().equals(productId)).collect(Collectors.toList());

        Long id = productList.get(0).getProductCustomer().getCustomerId();
        return customerRepository.findOne(id);


    }
}
