package pl.lamasoft.service.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.lamasoft.entity.UserAccount;
import pl.lamasoft.repository.UserAccountRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserDetailService implements org.springframework.security.core.userdetails.UserDetailsService {



    @Autowired
    private UserAccountRepository userAccountRepository;




    @Override
    public UserDetails loadUserByUsername(String userAccountEmail) {

        // we load by email, not username, but the method name must be preserved

        UserAccount userAccount = userAccountRepository.findByUserAccountEmail(userAccountEmail);
        if (Objects.isNull(userAccount)) {
            throw new UsernameNotFoundException(userAccountEmail);
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(userAccount.getUserAccountRole().toString()));




        return new User(userAccount.getUserAccountEmail(), userAccount.getUserAccountPassword(), authorities);
    }


}
