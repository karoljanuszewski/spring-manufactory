package pl.lamasoft.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


/**
 * setters and getters used in :
 *      - pl.lamasoft.service.Product package in ProductCommandService.class
 *
 * if the Product class is changed, f.i. - new attributes added/removed, changing methods in classes above is mandatory
 */

@Entity
@Table(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_ID")
    private Long productId;


    @NotEmpty
    @Column(name="PRODUCT_NAME")
    private String productName;


    @NotEmpty
    @Column(name="PRODUCT_DESC")
    private String productDesc;

    @Column (name="PRODUCT_QUANTITY")
    private int productQuantity;

    //TODO add appropriate annotation-constraint, to handle empty/null/ input
    @Column (name="PRODUCT_UNIT_PRICE")
    private BigDecimal productUnitPrice;



    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "purchaseProduct")
    private Set<Purchase> productPurchaseSet = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)                 // must be eager, otherwise, will not join on panel_products.jsp and respond with 500
    @JoinColumn(name = "CUSTOMER_CUSTOMER_ID",nullable = false)
    private Customer productCustomer;


    public Product() {
    }

    public Set<Purchase> getProductPurchaseSet() {
        return productPurchaseSet;
    }

    public void setProductPurchaseSet(Set<Purchase> productPurchaseSet) {
        this.productPurchaseSet = productPurchaseSet;
    }

    public Customer getProductCustomer() {
        return productCustomer;
    }

    public void setProductCustomer(Customer productCustomer) {
        this.productCustomer = productCustomer;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public BigDecimal getProductUnitPrice() {
        return productUnitPrice;
    }

    public void setProductUnitPrice(BigDecimal productUnitPrice) {
        this.productUnitPrice = productUnitPrice;
    }
}
