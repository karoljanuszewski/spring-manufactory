package pl.lamasoft.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PURCHASE")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PURCHASE_ID")
    private Long purchaseId;

    @Column(name = "PURCHASE_PRICE")
    private BigDecimal purchasePrice;   //cena ostateczna

    @Column(name = "PURCHASE_DISCOUNT")
    private BigDecimal purchaseDiscount;    //rabat podany w procentach (bez zaaki %)

    @Column(name = "PURCHASE_PRODUCT_UNIT_PRICE")
    private BigDecimal purchaseProductUnitPrice;    //cena jednostkowa wypadkowa

    @Column(name="PURCHASE_QUANTITY")
    private Long purchaseQuantity;          //ilosc do zamowienia

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_PRODUCT_ID",nullable = false)
    private Product purchaseProduct;        // produkt o ktory chodzi

    /*
    this attribute/column will we deleted/continued after consulting with client
     */
  /*  @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CART_CART_ID",nullable = false)
    private Cart purchaseCart;*/

    public Purchase() {
    }




    public Product getPurchaseProduct() {

        return purchaseProduct;
    }

    public void setPurchaseProduct(Product purchaseProduct) {
        this.purchaseProduct = purchaseProduct;
    }

    public Long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public BigDecimal getPurchaseDiscount() {
        return purchaseDiscount;
    }

    public void setPurchaseDiscount(BigDecimal purchaseDiscount) {
        this.purchaseDiscount = purchaseDiscount;
    }

    public BigDecimal getPurchaseProductUnitPrice() {
        return purchaseProductUnitPrice;
    }

    public void setPurchaseProductUnitPrice(BigDecimal purchaseProductUnitPrice) {
        this.purchaseProductUnitPrice = purchaseProductUnitPrice;
    }



    public Long getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public void setPurchaseQuantity(Long purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }

}
