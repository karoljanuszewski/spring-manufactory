package pl.lamasoft.entity;


import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * setters and getters used in :
 * - pl.lamasoft.service.Customer package in CustomerCommandService.class
 * <p>
 * if the Customer class is changed, f.i. - new attributes added, changing methods above is mandatory
 */

@Entity
@Table(name = "CUSTOMER")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    @NotEmpty
    @Column(name = "CUSTOMER_NAME")
    private String customerName;

    @NotEmpty
    @Column(name = "CUSTOMER_ADDRESS")
    private String customerAddress;


    @NotEmpty
    @Column(name = "CUSTOMER_TEL")
    private String customerTel;

    @OneToOne(fetch = FetchType.EAGER)          //TODO lazy to eager jeśli coś będzie nie teges w edit Customer
    @JoinColumn(name = "USER_ACCOUNT_ID", nullable = false)
    private UserAccount userAccount;


    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "productCustomer")
    private Set<Product> customerProducts = new HashSet<>();


    public Customer() {
    }

    public Set<Product> getCustomerProducts() {
        return customerProducts;
    }

    public void setCustomerProducts(Set<Product> customerProducts) {
        this.customerProducts = customerProducts;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerTel() {
        return customerTel;
    }

    public void setCustomerTel(String customerTel) {
        this.customerTel = customerTel;
    }

    @Override
    public String toString() {
        return String.format("%s",customerName);
    }
}
