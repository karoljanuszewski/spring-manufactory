package pl.lamasoft.entity;


import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table(name = "USER_ACCOUNT")
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ACCOUNT_ID")
    private Long userAccountId;

    @NotEmpty
    @Column(name = "USER_ACCOUNT_EMAIL", unique = true)
    private String userAccountEmail;

    @NotEmpty
    @Column(name = "USER_ACCOUNT_PASSWORD")
    private String userAccountPassword;

    // does not have to be @NotNull/@NotEmpty, handled in CustomerCommandService by setting a role to Role.ROLE_USER
    @Enumerated(EnumType.STRING)
    @Column(name = "USER_ACCOUNT_ROLE")
    private Role userAccountRole;


    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "userAccount")
    private Customer customer;

    public UserAccount() {
    }

    public Role getUserAccountRole() {
        return userAccountRole;
    }

    public void setUserAccountRole(Role userAccountRole) {
        this.userAccountRole = userAccountRole;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getUserAccountEmail() {
        return userAccountEmail;
    }

    public void setUserAccountEmail(String userAccountEmail) {
        this.userAccountEmail = userAccountEmail;
    }

    public String getUserAccountPassword() {
        return userAccountPassword;
    }

    public void setUserAccountPassword(String userAccountPassword) {
        this.userAccountPassword = userAccountPassword;
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
