insert into USER_ACCOUNT (USER_ACCOUNT_EMAIL, USER_ACCOUNT_PASSWORD, USER_ACCOUNT_ROLE) values ('admin@admin.pl', 'admin', 'ROLE_ADMIN');
insert into USER_ACCOUNT (USER_ACCOUNT_EMAIL, USER_ACCOUNT_PASSWORD, USER_ACCOUNT_ROLE) values ('anna@anna.pl', 'anna', 'ROLE_USER');
insert into USER_ACCOUNT (USER_ACCOUNT_EMAIL, USER_ACCOUNT_PASSWORD, USER_ACCOUNT_ROLE) values ('aga@aga.pl', 'aga', 'ROLE_USER');
insert into USER_ACCOUNT (USER_ACCOUNT_EMAIL, USER_ACCOUNT_PASSWORD, USER_ACCOUNT_ROLE) values ('wiesiek@pudelek.pl', 'wiesiek', 'ROLE_USER');
insert into USER_ACCOUNT (USER_ACCOUNT_EMAIL, USER_ACCOUNT_PASSWORD, USER_ACCOUNT_ROLE) values ('janusz@o2.pl', 'janusz', 'ROLE_USER');


INSERT INTO `spring-manufactory`.`customer` (`CUSTOMER_ADDRESS`, `CUSTOMER_NAME`, `CUSTOMER_TEL`, `USER_ACCOUNT_ID`) VALUES ('al. Warszawska 14, 26-154 Radom, Polska', 'Radomska fabryka mebli', '506486985', '4');
INSERT INTO `spring-manufactory`.`customer` (`CUSTOMER_ADDRESS`,  `CUSTOMER_NAME`, `CUSTOMER_TEL`, `USER_ACCOUNT_ID`) VALUES ('al.Batalionow Chłopskich 3, 17-872 Muszyna, Polska', 'Muszynianka', '662148756', '5');


INSERT INTO `spring-manufactory`.`product` (`PRODUCT_DESC`, `PRODUCT_NAME`, `PRODUCT_QUANTITY`,`PRODUCT_UNIT_PRICE`, `CUSTOMER_CUSTOMER_ID`) VALUES ('nie wolno szlifowac na mokro!', 'sprezyna JD83-K','123', '1.3', '1');
INSERT INTO `spring-manufactory`.`product` (`PRODUCT_DESC`, `PRODUCT_NAME`, `PRODUCT_QUANTITY`,`PRODUCT_UNIT_PRICE`, `CUSTOMER_CUSTOMER_ID`) VALUES ('kolorowac tylko na zolto', 'sprezyna He73-90','232', '2.3', '1');
INSERT INTO `spring-manufactory`.`product` (`PRODUCT_DESC`, `PRODUCT_NAME`, `PRODUCT_QUANTITY`, `PRODUCT_UNIT_PRICE`, `CUSTOMER_CUSTOMER_ID`) VALUES ('moczyc w occie na noc', 'sprezynka 12-333NHG', '100', '7.2', '1');


INSERT INTO `spring-manufactory`.`product` (`PRODUCT_DESC`, `PRODUCT_NAME`, `PRODUCT_QUANTITY`, `PRODUCT_UNIT_PRICE`, `CUSTOMER_CUSTOMER_ID`) VALUES ('po zmierzchu zamykac w garazu', 'ksztaltka JDBC123', '2', '0.0002', '2');
INSERT INTO `spring-manufactory`.`product` (`PRODUCT_DESC`, `PRODUCT_NAME`, `PRODUCT_QUANTITY`, `PRODUCT_UNIT_PRICE`, `CUSTOMER_CUSTOMER_ID`) VALUES ('nie palic w piecu', 'sprezyna uniwersalna', '1000', '0.01', '2');

INSERT INTO `spring-manufactory`.`purchase` (`PURCHASE_PRODUCT_UNIT_PRICE`, `PURCHASE_QUANTITY`, `PRODUCT_PRODUCT_ID`) VALUES ('10', '1000', '1');
INSERT INTO `spring-manufactory`.`purchase` (`PURCHASE_PRODUCT_UNIT_PRICE`, `PURCHASE_QUANTITY`, `PRODUCT_PRODUCT_ID`) VALUES ('20', '2000', '2');