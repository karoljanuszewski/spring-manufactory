package pl.lamasoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.lamasoft.entity.UserAccount;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount,Long>{

    UserAccount findByUserAccountEmail(String userAccountEmail);
}
